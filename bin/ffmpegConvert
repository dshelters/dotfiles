#!/usr/bin/env bash

# Use ffmpeg to convert media files.

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
if [[ ! -z ${DEBUG_MODE+x} ]]; then
    set -o xtrace
    set -o functrace
fi

# Check for ffmpeg and exit with error message if not found.
if ! command -v ffmpeg >/dev/null; then
    echo "ffmpeg not found."
    exit 1
fi

# Name that was used to invoke this script. Used to determine which commands below to run.
NAME=$(basename "$0")

# Full path of the file to be converted.
INPUT=$(realpath "$1")

# Full path of the input file with the extension stripped.
BASE="${INPUT%.*}"

if [ ! -f "$INPUT" ]; then
    echo "Input file does not exist, cannot continue."
    exit 1
fi

if [[ $NAME == 'flac2mp3' ]]; then
    OUTPUT="$BASE.mp3"
    ffmpeg -i "$INPUT" -ab 320k -map_metadata 0 -id3v2_version 3 "$OUTPUT"

elif [[ $NAME == 'gif2webm' ]]; then
    OUTPUT="$BASE.webm"
    ffmpeg -loglevel quiet -i "$INPUT" -c:v libvpx -crf 12 -b:v 500K  -auto-alt-ref 0 -y "$OUTPUT"
    rm "$INPUT"

elif [[ $NAME == 'wmv2mp3' ]]; then
    OUTPUT="$BASE.mp3"
    ffmpeg -i "$INPUT" -ab 320k -map_metadata 0 -id3v2_version 3 "$OUTPUT"
fi