#!/usr/bin/env bash

# Create a git bundle (see man git-bundle) for a repo and upload it to an s3 bucket
# specified by the GITHUB_BACKUP_BUCKET environment variable.

set -o errexit errtrace nounset pipefail

git up

for branch in $(git branch --remotes | grep -v HEAD);
do
  branchname=${branch/origin\//}
  git checkout --quiet "$branchname"
done

backupDir="$HOME/github-backups"
currentDir=$(basename "$(pwd)")
currentDate=$(date --iso-8601)
backupFilename="$currentDir-$currentDate.bundle"
backupPath="$backupDir/$backupFilename"
backupBucket="$GITHUB_BACKUP_BUCKET"

if [[ -z $backupBucket ]]; then
  echo "GITHUB_BACKUP_BUCKET environment variable not set, cannot continue."
  exit 1
fi

mkdir -p "$backupDir"

git bundle create "$backupPath" --all
git bundle verify "$backupPath"

cd "$backupDir" || exit
aws s3 sync . "s3://$backupBucket"

aws s3 ls --human-readable "s3://$backupBucket/$backupFilename"
