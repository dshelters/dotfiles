This is a fork of https://gitlab.com/bradymitch/dotfiles

# dotfiles

This is the latest iteration of my dotfiles. Much of the stuff here will work as-is in OSX, but I no longer use OSX regularly so no guarantees. If you find bugs, issues and especially merge requests are welcome!

## Installing

I'm using [fresh](https://github.com/freshshell/fresh) to manage my dotfiles.

To install:

```
FRESH_LOCAL_SOURCE=https://gitlab.com/bradymitch/dotfiles bash <(curl -sL get.freshshell.com)
```

If you have vim installed, the vim plugins I use will also be installed.

## Repo organization

| folder/file     | description                                                  |
| --------------- | ------------------------------------------------------------ |
| bin             | Various scripts I've written, will be symlinked in $HOME/bin |
| config          | Files in are symlinked from $HOME prefixed with a `.`        |
| shell/base.sh   | This becomes $HOME/.bashrc and $HOME/.bash_profile           |
| shell/alias.sh  | Bash aliases go here                                         |
| shell/config    | Config for bash and a number of external programs            |
| shell/functions | Bash functions go here                                       |

## Credits

I've copied from so many dotfile repos over the years that I don't know
where most if it came from at this point, but here are some of the places
I've taken ideas and code from:

- https://github.com/mitchellmckenna/dotfiles
- https://github.com/emilis/emilis-config
- https://github.com/whiteinge/dotfiles
- https://github.com/jasoncodes/dotfiles
- http://ruslanspivak.com/2010/06/02/urlencode-and-urldecode-from-a-command-line/

If you recognize something and know where it came from, let me know and
I'll update this list.
