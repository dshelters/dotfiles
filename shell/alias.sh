# Display environment variables sorted
alias envs="env | sort"

# Always show file size in human readable units
alias du="du -h"
alias df="df -h -x squashfs"

# Enable aliases to be sudo'ed
alias sudo='sudo '
alias sudp='sudo '

# Always include user and hostname in notifications from noti
if command -v noti &>/dev/null; then
    alias n='noti -m "`whoami`@`hostname`: Done!"'
fi

# List active ssh connections
alias sshl="ps -Af | sed -n '1p; /sed -n/d; /ssh:/p;'"

# Fast download
if command -v aria2c &>/dev/null; then
    alias fdl="aria2c --max-connection-per-server=16 --split=16 --file-allocation=none"
fi

# pbcopy exists on mac, on linux we have xclip. Make it easier to use.
if ! command -v pbcopy &>/dev/null && command -v xclip &>/dev/null; then
    alias pbcopy="xclip -selection c"
fi

# pbpaste exists on mac, on linux we have xclip. Make it easier to use.
if ! command -v pbpaste &>/dev/null && command -v xclip &>/dev/null;  then
    alias pbpaste="xclip -selection c -o"
fi

# Grab screenshot in Gnome
alias scg="gnome-screenshot -a"

# vsCode was removed from path for some reason
alias code="/usr/share/code/bin/code"

# Add k8s lens tool to path
alias lens="$HOME/apps/Lens-4.2.4.x86_64.AppImage"
