#!/usr/bin/env bash

# Only continue if we're in an interactive session
[[ -z "$PS1" ]] && return

# shellcheck disable=SC1090
# Source ~/.local-pre for machine-specific config
[[ -s "$HOME/.local-pre" ]] && source "$HOME/.local-pre"

# Configuration for fresh
export FRESH_RCFILE="$HOME/.dotfiles/freshrc"

# shellcheck disable=SC1090
# Get everything installed/configured via fresh
[[ -s "$HOME/.fresh/build/shell.sh" ]] && source "$HOME/.fresh/build/shell.sh"

# shellcheck disable=SC1090
# Source ~/.local-post for machine-specific config
[[ -s "$HOME/.local-post" ]] && source "$HOME/.local-post"
