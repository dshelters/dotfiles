#!/usr/bin/env bash
# shellcheck disable=SC1091

if [[ -s "$HOME/.dotfiles/nvm_envs" ]]; then
    # Source NPM_TOKEN and other env vars for nvm
    source "$HOME/.dotfiles/nvm_envs"
fi
# -----------------------------------------------------
# nvm: Node Version Manager
# https://github.com/nvm-sh/nvm
# -----------------------------------------------------

if ! type -t nvm &>/dev/null; then
    NVM_DIR="${NVM_DIR:-$HOME/.nvm}"
    if [[ -d "$NVM_DIR" ]]; then
        [[ -s "$NVM_DIR/nvm.sh" ]] && source "$NVM_DIR/nvm.sh"
        [[ -s "$NVM_DIR/bash_completion" ]] && source "$NVM_DIR/bash_completion"
    fi
fi
