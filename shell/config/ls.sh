
# -----------------------------------------------------
# ls
# -----------------------------------------------------
# Colors for ls
export LS_COLORS='no=00:fi=00:di=36:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ex=01;32'

# Detect which `ls` flavor is in use
if ls --color >/dev/null 2>&1; then
    # GNU ls
    LS_COLOR_FLAG="--color"
else
    # BSD ls
    LS_COLOR_FLAG="-G"
fi

# Always use color output for `ls`
# shellcheck disable=SC2139
alias ls="command ls ${LS_COLOR_FLAG}"

# List files colorized in long format
# shellcheck disable=SC2139
alias ll="ls -lhF ${LS_COLOR_FLAG}"

# List all files colorized in long format, including dot files
# shellcheck disable=SC2139
alias la="ls -lhFa ${LS_COLOR_FLAG}"

# List only directories
# shellcheck disable=SC2139
alias lsd="ls -lF ${LS_COLOR_FLAG} | grep --color=never '^d'"
