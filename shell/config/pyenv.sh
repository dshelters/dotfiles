# -----------------------------------------------------
# pyenv: Python version management
# https://github.com/pyenv/pyenv
# -----------------------------------------------------
pathmunge "$HOME/.pyenv/bin"
if command -v pyenv &>/dev/null; then
    export WORKON_HOME=${WORKON_HOME:-$HOME/.ve}
    export PROJECT_HOME=${PROJECT_HOME:-$HOME/code}
    export PYENV_ROOT=$HOME/.pyenv
    export PYENV_VIRTUALENV_DISABLE_PROMPT=1
    export PYENV_VERSION=${PYTHON_VERSION:-3.6.8}

    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    pyenv virtualenvwrapper_lazy
fi