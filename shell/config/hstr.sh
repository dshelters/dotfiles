# -----------------------------------------------------
# hstr: Bash and zsh shell history suggest box
# https://github.com/dvorka/hstr
# -----------------------------------------------------
if command -v hh &>/dev/null; then
    # get more colors
    export HSTR_CONFIG=hicolor

    # if this is interactive shell, then bind hh to Ctrl-r (for Vi mode check doc)
    if [[ $- =~ .*i.* ]]; then bind '"\C-r": "\C-a hh -- \C-j"'; fi

    # if this is interactive shell, then bind 'kill last command' to Ctrl-x k
    if [[ $- =~ .*i.* ]]; then bind '"\C-xk": "\C-a hh -k \C-j"'; fi
fi