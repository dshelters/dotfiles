# -----------------------------------------------------
# pet: command-line snippet manager
# https://github.com/knqyf263/pet
# -----------------------------------------------------
if command -v pet &>/dev/null; then
    function pet-select() {
        BUFFER=$(pet search --query "$READLINE_LINE")
        READLINE_LINE=$BUFFER
        READLINE_POINT=${#BUFFER}
    }

    bind -x '"\C-x\C-r": pet-select'
    alias prev='cmd=$(fc -ln -2 -2 | sed -E "s/^[ \\t]*//; s/[ ]*\$//"); eval pet new $(printf %q "$cmd")'
fi
