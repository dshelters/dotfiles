# -----------------------------------------------------
# k8s
# -----------------------------------------------------

# If kubectl not found, nothing here will be useful.
if command -v kubectl &>/dev/null; then
    # shellcheck disable=SC1090
    source <(kubectl completion bash)

    alias kuebctl=kubectl
    alias kc=kubectl

    if type _complete_alias &>/dev/null; then
        complete -F _complete_alias kc
        complete -F _complete_alias kuebctl
    fi

    if command -v kubectx &>/dev/null; then
        alias kctx=kubectx

        _kube_contexts() {
            local CURR_ARG
            CURR_ARG=${COMP_WORDS[COMP_CWORD]}
            mapfile -t COMPREPLY < <(compgen -W "- $(kubectl config get-contexts --output='name')" -- "$CURR_ARG")
        }

        complete -F _kube_contexts kubectx kctx

        if type _fzf_complete &>/dev/null; then
            _fzf_complete_kctx() {
                _fzf_complete "--multi --reverse --ansi" "$@" < <(kctx)
            }

            [[ -n "$BASH" ]] && complete -F _fzf_complete_kctx -o default -o bashdefault kctx
        fi

        if type _complete_alias &>/dev/null; then
            complete -F _complete_alias kctx
        fi

    fi

    if command -v kubens &>/dev/null; then
        alias kns=kubens

        _kube_namespaces() {
            local CURR_ARG
            CURR_ARG=${COMP_WORDS[COMP_CWORD]}
            mapfile -t COMPREPLY < <(compgen -W "- $(kubectl get namespaces -o=jsonpath='{range .items[*].metadata.name}{@}{"\n"}{end}')" -- "$CURR_ARG")
        }

        complete -F _kube_namespaces kubens kns

        if type _fzf_complete &>/dev/null; then
            _fzf_complete_kns() {
                _fzf_complete "--multi --reverse --ansi" "$@" < <(kns)
            }
            [[ -n "$BASH" ]] && complete -F _fzf_complete_kns -o default -o bashdefault kns
        fi

        if type _complete_alias &>/dev/null; then
            complete -F _complete_alias kns
        fi

    fi

    # Use fzf to select a pod and container, and start bash/ash/sh session in the container.
    # Adapted from: https://gist.github.com/jondlm/35cbf0363eb925e2eff6ff86c0a30992
    function kdebug() {
        if ! command -v kubectl &>/dev/null; then
            echo "Please install \`kubectl\` before running this command (https://kubernetes.io/docs/tasks/tools/install-kubectl/)"
            return 1
        fi
        if ! command -v fzf &>/dev/null; then
            echo "Please install \`fzf\` before running this command (https://github.com/junegunn/fzf#installation)"
            return 1
        fi

        local selection
        local namespace
        local pod
        local containers
        local container_count
        local container

        selection=$(kubectl get pods --all-namespaces | grep -v 'NAMESPACE' | fzf --header "Select a pod...")

        if [[ $selection ]]; then

            namespace=$(echo "$selection" | awk '{ print $1 }')
            pod=$(echo "$selection" | awk '{ print $2 }')
            containers=$(kubectl -n "$namespace" get pods "$pod" -o jsonpath='{range .spec.containers[*]}{@.name}{"\n"}{end}')
            container_count=$(echo "$containers" | wc -l)

            if [[ "$container_count" -gt "1" ]]; then
                container=$(echo "$containers" | fzf --header "Select a container...")
            else
                container=$containers
            fi

            for shell in bash ash sh; do
                if kubectl -n "$namespace" exec -it "$pod" -c "$container" which $shell &>/dev/null; then
                    kubectl -n "$namespace" exec -it "$pod" -c "$container" -- "$shell"
                    break
                fi
            done
        fi
    }

    # Get all containers running in k8s
    function get-containers() {
        kubectl get pods -o json | jq -r '.items[].spec.containers[] | [.name, .image] | @tsv' | column -t
    }

    if command -v helm &>/dev/null; then
        # shellcheck disable=SC1090
        source <(helm completion bash)
    fi
fi