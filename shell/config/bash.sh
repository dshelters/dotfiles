# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Enable recursive globbing
shopt -s globstar

# cd into directory when directory name given as command
shopt -s autocd

# Autocorrect typos in path names when using `cd`
shopt -s cdspell

# Check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Attempt spelling correction on directory names during word completion
shopt -s dirspell

# Where to look for directories when using cd
export CDPATH=".:$HOME:$HOME/code/"


# -----------------------------------------------------
# bash tab completion
# -----------------------------------------------------

if [[ -s /etc/bash_completion ]]; then
    source /etc/bash_completion
fi

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
if [[ -s "$HOME/.ssh/config" ]]; then
    complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2 | tr ' ' '\n')" scp sftp ssh
fi
