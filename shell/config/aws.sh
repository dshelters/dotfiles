# -----------------------------------------------------
# AWS
# -----------------------------------------------------

# Get current aws profile
function get-aws-profile() {
    echo "$AWS_PROFILE"
}

# Set current aws profile
function set-aws-profile() {
    export AWS_PROFILE="$1"
}

# Get param value from SSM parameter store
function get-param() {
    if [[ -z $1 ]]; then
        echo "Must supply a parameter name."
        echo "Usage: get-param <name>"
        return 1
    fi

    local NAME=${1//__/\/}
    aws ssm get-parameter --with-decryption --name "$NAME" --query 'Parameter.Value' --output text
}

# Get params in path from SSM parameter store
function get-path-params() {
    if [[ -z $1 ]]; then
        echo "Must supply a path."
        echo "Usage: get-path-param <path>"
        return 1
    fi

    aws ssm get-parameters-by-path --path "$1" --with-decryption --recursive | jq -r '.Parameters[] | [.Name,.Value] | @tsv' | column -ts $'\t' -c 200 | sort
}

# Add parameter to SSM parameter store
function set-param() {
    local NAME=${1//__/\/}
    local VALUE="$2"
    local TYPE="${3:-SecureString}"
    local KEY="${4:-$DEFAULT_SSM_KEY}"

    if [[ -z "$KEY" ]]; then
        echo "Must either provide the keyname as a parameter or set DEFAULT_SSM_KEY env variable."
        return 1
    fi

    if [[ -z $NAME ]] || [[ -z $VALUE ]]; then
        echo "Name and value must both be specified."
        echo "Usage: add-param <name> <value> [type] [key]"
        return 1
    fi

    if [[ $TYPE == "SecureString" ]]; then
        aws ssm put-parameter --type "$TYPE" --name "$NAME" --key-id "$KEY" --value "$VALUE" --overwrite
    else
        aws ssm put-parameter --type "$TYPE" --name "$NAME" --value "$VALUE" --overwrite
    fi
}

# command completion for aws cli
# shellcheck disable=2046
if command -v aws_completer &>/dev/null; then
    complete -C $(command -v aws_completer) aws
fi

# Tab completion for set-aws-profile
_set-aws-profile_completions() {
    if [[ "${#COMP_WORDS[@]}" != "2" ]]; then return; fi

    # keep the suggestions in a local variable
    local suggestions=($(compgen -W "$(grep '\[' ~/.aws/config | sed 's/\[\|\]\|profile //g')" -- "${COMP_WORDS[1]}"))

    if [[ "${#suggestions[@]}" == "1" ]]; then
        # if there's only one match, we remove the command literal
        # to proceed with the automatic completion of the number
        local number=$(echo ${suggestions[0]/%\ */})
        COMPREPLY=("$number")
    else
        # more than one suggestions resolved,
        # respond with the suggestions intact
        COMPREPLY=("${suggestions[@]}")
    fi
}

complete -F _set-aws-profile_completions set-aws-profile