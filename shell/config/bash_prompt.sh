prompt_last_exit() {
    # Echo out the exit status of the previous command.
    #
    # In any other situation, this function would be overkill. Doing this here because it lets me use
    # printf formatting for the prompt commmand, making it significantly more readable than alternatives.
    echo $?
}

prompt_aws() {
    if [[ -n "$AWS_PROFILE" ]]; then
        echo " [aws:$AWS_PROFILE] "
    else
        echo ''
    fi
}

prompt_kube() {
    KUBE_INFO=''

    if type -t kube_ps1 &>/dev/null && command -v kubectl &>/dev/null; then
        export KUBE_PS1_SYMBOL_ENABLE=false
        export KUBE_PS1_PREFIX='['
        export KUBE_PS1_SUFFIX=']'

        KUBE_INFO="$(kube_ps1)"
    fi
    echo "$KUBE_INFO"
}

prompt_git() {
    local STATUS=''
    local BRANCH_NAME=''

    # Check if the current directory is in a Git repository.
    if [[ $(git rev-parse --is-inside-work-tree &>/dev/null && echo "${?}") == '0' ]]; then
        # Get the short symbolic ref.
        # If HEAD isn’t a symbolic ref, get the short SHA for the latest commit
        # Otherwise, just give up.
        BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)

        # check if the current directory is in .git before running git checks
        if [[ "$(git rev-parse --is-inside-git-dir 2>/dev/null)" == 'false' ]]; then
            # Ensure the index is up to date.
            git update-index --really-refresh -q &>/dev/null

            # Check for uncommitted changes in the index.
            if ! git diff --quiet --ignore-submodules --cached; then
                STATUS+='+'
            fi

            # Check for unstaged changes.
            if ! git diff-files --quiet --ignore-submodules --; then
                STATUS+='!'
            fi

            # Check for untracked files.
            if [[ -n "$(git ls-files --others --exclude-standard)" ]]; then
                STATUS+='?'
            fi

            # Check for stashed files.
            if git rev-parse --verify refs/stash &>/dev/null; then
                STATUS+='$'
            fi

            # Check for missing upstream on the current branch.
            if ! git rev-parse --abbrev-ref --symbolic-full-name "$BRANCH_NAME@{u}" &>/dev/null; then
                STATUS+='@'
            fi
        fi

        [[ -n "${STATUS}" ]] && STATUS=" [${STATUS}]"

        echo -e "on ${VIOLET}${BRANCH_NAME}${BLUE}${STATUS}${RESET}"
    else
        return
    fi
}

active_prompt() {
    # Highlight the user name when logged in as root.
    if [[ "${USER}" == "root" ]]; then
        local USER_STYLE="${RED}"
    else
        local USER_STYLE="${ORANGE}"
    fi

    # Highlight the hostname when connected via SSH.
    if [[ "${SSH_TTY}" ]]; then
        local HOST_STYLE="${BOLD}${RED}"
    else
        local HOST_STYLE="${YELLOW}"
    fi

    local LAST_EXIT="\$(prompt_last_exit)"
    local TIME="\\t"
    local AWS="\001${BLUE}\002\$(prompt_aws)\001${RESET}\002"
    local KUBE="\$(prompt_kube)"
    local USER="\001${USER_STYLE}\002\\u\001${RESET}\002"
    local HOST="\001${HOST_STYLE}\002\\h\001${RESET}\002"
    local DIR="\001${GREEN}\002\\w\001${RESET}\002"
    local GIT="\$(prompt_git)"

    # Sample output
    # [0] [19:56:13] [aws:personal] [k8s-cluster-name:default] user at hostname in ~/.dotfiles on master [!]
    printf "[%s] [%s]%s%s %s at %s in %s %s\\n$ " \
        "$LAST_EXIT" "$TIME" "$AWS" "$KUBE" "$USER" "$HOST" "$DIR" "$GIT"
}

PS1=$(active_prompt)
export PS1

PS2="\\[${YELLOW}\\]→ \\[${RESET}\\]"
export PS2

# Set terminal title to user@hostname:currentWorkingDirectory
# PROMPT_COMMAND is executed before the prompt is rendered, so it's always up to date.

function prompt_command_wrapper(){
    echo -ne "\033]0;$USER@$(hostname -s):$(dirs)\007"
}

PROMPT_COMMAND="prompt_command_wrapper;$PROMPT_COMMAND"