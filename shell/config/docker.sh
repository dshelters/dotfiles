# -----------------------------------------------------
# Docker
# -----------------------------------------------------

# Common variable definitions
DOCKER_IMAGE_BACKUP_DIR="${HOME}/docker-image-backups/"

# Customized docker ps output
alias dps='docker ps --format "table {{.Names}}\t{{.Status}}\t{{.Ports}}"'

# Shortcut for docker images
alias di='docker images --format "table {{ .Repository }}:{{ .Tag }}\t{{ .ID }}\t{{ .CreatedSince }}\t{{ .Size }}"'

# Shortcut for getting names of containers
alias dcn='docker ps --format {{.Names}}'

# Docker logs for service
alias dl='docker logs -f'

# Show ports for all running docker containers
function dpo() {
    for containerId in $(dcn); do
        docker inspect -f "{{ .Name }}" "$containerId" | sed 's#/##'
        docker port "$containerId"
        echo
    done
}

# Run a shell on the given container.
function de() {
    if [ -z "$1" ]; then
        echo "container name must be provided."
        echo "Usage: ${FUNCNAME[0]} <container name>"
        return 1
    fi

    local PS_OUTPUT
    PS_OUTPUT=$(dcn | grep "$1" 2>/dev/null)

    # shellcheck disable=SC2181
    if [[ $? -ne 0 || -z $PS_OUTPUT ]]; then
        echo "specified container does not exist, or is not running."
        echo "Output of docker ps: "
        docker ps
        return 1
    fi

    # Support for multiple shells
    for shell in bash ash sh; do
        if docker exec -it "$1" which "$shell" &>/dev/null; then
            docker exec -e TERM=xterm -it "$1" "$shell" -c "export COLUMNS=$(tput cols) && export LINES=$(tput lines) && exec $shell"
            break 2
        fi
    done
}

# Command completion for de function
_de_complete() {
    local CUR=${COMP_WORDS[COMP_CWORD]}
    CONTAINERS=$(dcn)
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$CONTAINERS" -- "$CUR"))

    return 0
}

complete -F _de_complete de
complete -F _de_complete dl

# Delete stopped containers, dangling images, networks, volumes and build cache
alias dcu='docker system prune --volumes --force && docker network create net'

# Delete all containers
function drmac() {
    echo "This command will remove ALL docker containers."
    read -p "Are you sure you want to continue? " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        # shellcheck disable=SC2046
        docker rm -fv "$(docker stop $(docker ps -aq))"
    fi
}

function dlsib() {
    find "$DOCKER_IMAGE_BACKUP_DIR" -name "*.tar" -type f -printf "%f\\n"
}

#  Import docker image from tar file
function dii() {
    if [ -z "$1" ]; then
        echo "Must specify the docker image file to import".
        return 1
    fi

    local FILE=${DOCKER_IMAGE_BACKUP_DIR}$1

    if [ -e "$FILE" ]; then
        docker load -i "$FILE"
    else
        echo "$FILE does not exist, cannot import."
        echo "The following images can be restored: "
        echo
        dlsib
        return 1
    fi
}

# Command completion for dii
_dii_complete() {
    local cur=${COMP_WORDS[COMP_CWORD]}
    local FILES
    FILES=$(dlsib)
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$FILES" -- "$cur"))

    return 0
}

complete -F _dii_complete dii

# Export docker images
function diex() {
    if [ -z "$1" ]; then
        echo "image name must be provided."
        echo "Usage: ${FUNCNAME[0]} <image name>"
        return 1
    fi

    if [ ! -d "$DOCKER_IMAGE_BACKUP_DIR" ]; then
        mkdir "$DOCKER_IMAGE_BACKUP_DIR"
    fi

    local DATESTR
    DATESTR=$(date +%F.%N)
    local IMG_CLEAN=${1//\//-}
    local FILENAME="$IMG_CLEAN-$DATESTR.tar"

    docker save -o "$DOCKER_IMAGE_BACKUP_DIR/$FILENAME" "$1"
}

# Truncate docker logs
function dlt() {
    local LOGPATH
    LOGPATH=$(docker inspect "$1" | jq -r .[0].LogPath)
    cat /dev/null >"$LOGPATH"
}

# Start shell in new docker container using specified image.
function dr() {
    local IMAGE=$1
    local APP

    APP=$(echo "$IMAGE" | cut -d/ -f2 | cut -d: -f1)

    for SHELL in bash ash sh; do
        if docker run --rm -ti --init --name "$APP" --network net --network-alias "$APP" --hostname "$APP" "$IMAGE" "$SHELL"; then
            break 2
        fi
    done
}

# Keep latest docker image for given repo
function dkl() {
    docker images "$1" -q | tail -n +2 | uniq | xargs docker rmi -f
}
