# -----------------------------------------------------
# Colors!
# -----------------------------------------------------

# As of shellcheck 0.6.0 there is no way to disable a rule for one section of a file and leave it enabled elsewhere.
# Since I don't care if all color variables are used, I'm diabling the rule for every variable.

if tput setaf 1 &>/dev/null; then
    tput sgr0 # reset colors
    # shellcheck disable=2034
    BOLD=$(tput bold)
    # shellcheck disable=2034
    RESET=$(tput sgr0)
    # Solarized colors, taken from http://git.io/solarized-colors.
    # shellcheck disable=2034
    BLACK=$(tput setaf 0)
    # shellcheck disable=2034
    BLUE=$(tput setaf 33)
    # shellcheck disable=2034
    CYAN=$(tput setaf 37)
    # shellcheck disable=2034
    GREEN=$(tput setaf 64)
    # shellcheck disable=2034
    ORANGE=$(tput setaf 166)
    # shellcheck disable=2034
    PURPLE=$(tput setaf 125)
    # shellcheck disable=2034
    RED=$(tput setaf 124)
    # shellcheck disable=2034
    VIOLET=$(tput setaf 61)
    # shellcheck disable=2034
    WHITE=$(tput setaf 15)
    # shellcheck disable=2034
    YELLOW=$(tput setaf 136)
else
    # shellcheck disable=2034
    BOLD=''
    # shellcheck disable=2034
    RESET="\\e[0m"
    # shellcheck disable=2034
    BLACK="\\e[1;30m"
    # shellcheck disable=2034
    BLUE="\\e[1;34m"
    # shellcheck disable=2034
    CYAN="\\e[1;36m"
    # shellcheck disable=2034
    GREEN="\\e[1;32m"
    # shellcheck disable=2034
    ORANGE="\\e[1;33m"
    # shellcheck disable=2034
    PURPLE="\\e[1;35m"
    # shellcheck disable=2034
    RED="\\e[1;31m"
    # shellcheck disable=2034
    VIOLET="\\e[1;35m"
    # shellcheck disable=2034
    WHITE="\\e[1;37m"
    # shellcheck disable=2034
    YELLOW="\\e[1;33m"
fi
