# Open github url for repo
function gh() {
    local WORKING_DIR="${1-$(pwd)}"
    local URL

    URL=$(git -C "$WORKING_DIR" remote get-url origin | sed 's#git@github.com:#https://github.com/#')

    if [[ -z "$URL" ]]; then
        echo "Unable to get github URL for git repo, is $WORKING_DIR a git repo?"
        return
    fi

    python3 -m webbrowser "$URL" &>/dev/null
}

# Git shortcuts
is_in_git_repo() {
  git rev-parse HEAD > /dev/null 2>&1
}

gs() {
  is_in_git_repo &&
    git -c color.status=always status --short |
    fzf --height 40% -m --ansi --nth 2..,.. | awk '{print $2}'
}

gb() {
  is_in_git_repo &&
    git branch -vv --color=always | grep -v '/HEAD\s' |
    fzf --height 40% --reverse --ansi --multi --tac | sed 's/^..//' | awk '{print $1}' |
    sed 's#^remotes/[^/]*/##'
}

gt() {
  is_in_git_repo &&
    git tag --sort -version:refname |
    fzf --height 40% --multi
}

gh() {
  is_in_git_repo &&
    git log --date=short --format="%C(green)%C(bold)%cd %C(auto)%h%d %s (%an)" --graph |
    fzf --height 40% --ansi --no-sort --reverse --multi | grep -o '[a-f0-9]\{7,\}'
}

gr() {
  is_in_git_repo &&
    git remote -v | awk '{print $1 " " $2}' | uniq |
    fzf --height 40% --tac | awk '{print $1}'
}
bind '"\er": redraw-current-line'

### CTRL-G CTRL-S - Files listed in git status
bind '"\C-g\C-s": "$(gs)\e\C-e\er"'

### CTRL-G CTRL-B - Branches
bind '"\C-g\C-b": "$(gb)\e\C-e\er"'

### CTRL-G CTRL-T - Tags
bind '"\C-g\C-t": "$(gt)\e\C-e\er"'

### CTRL-G CTRL-H - Commit hashes
bind '"\C-g\C-h": "$(gh)\e\C-e\er"'

### CTRL-G CTRL-R - Remotes
bind '"\C-g\C-r": "$(gr)\e\C-e\er"'

gcb() {
  BRANCH_NAME="$(gb)"
  if [[ "$BRANCH_NAME" == "" ]]; then
    echo "No branch found or selected"
  else
    echo "BRANCH_NAME is $BRANCH_NAME"
    git checkout "$BRANCH_NAME"
  fi
}

### Create link to staging site for git branch
staging_link() {
  APP=$(basename "$(pwd)")
  BRANCH=$(git rev-parse --abbrev-ref HEAD)
  BRANCH_WITHOUT_SLASHES=$(echo "$BRANCH" | tr "-" "/")
  LINK="https://$APP-$BRANCH_WITHOUT_SLASHES.stage.k8s.bloomboard.com"
  echo "Adding $LINK to clipboard"
  echo "$LINK" | xclip -sel clip
}
